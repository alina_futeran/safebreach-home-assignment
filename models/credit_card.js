var redis = require('redis');

var credit_cards_key = 'credit_cards';
var client = redis.createClient();

// Exported methods
var methods = {
    add         :   add,
    del_all     :   del_all,
    get_all     :   get_all,
    size        :   size,
};

function add(credit_card, callback) {
    client.rpush([credit_cards_key, credit_card], function (err, reply) {
            return size(callback);
        });
    }

function del_all(callback) {
    client.del(credit_cards_key, function (err, reply) {
            return callback(err, reply);
        });
    }

function get_all(callback) {
    client.lrange(credit_cards_key, 0, -1, function (err, reply) {
            return callback(err, reply);
        });
    }

function size(callback) {
    return client.lrange(credit_cards_key, 0 ,-1, function(err, reply) {
        if(reply) {
                reply = reply.length;
            }
        return callback(err, reply);
        });
    }

exports = module.exports = methods;
