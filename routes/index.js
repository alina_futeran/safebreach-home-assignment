var express = require('express');
var credit_cards = require('../models/credit_card');
var router = express.Router();


router.post('/cnc/assets', function(req, res){
    credit_cards.add(JSON.stringify(req.body.CreditCard), function(err, reply){
        if(err)
        {
            return res.send(err);
        }
        else {
            return res.send({NumAssets: reply});
        }
    })

});

router.get('/cnc/assets', function(req, res){
    credit_cards.get_all( function(err, reply) {
        if(err) {
            return res.send(err);

        }
        return res.send(reply);
        });
    });

router.delete('/cnc/assets', function(req, res){
    var old_credits = [];
    credit_cards.get_all(function( err, reply )
    {
        if(err)
        {
            return res.send(err);
        }
        old_credits = reply;
        credit_cards.del_all(function(err, reply)
        {
            if(err)
            {
                return res.send(err);
            }
            res.send(old_credits);
        });
    });
});


module.exports = router;


